export interface SearchCriteria {
  search: string;
  size: number;
  lastId: string;
}

export interface FindById {
  id: string;
}
