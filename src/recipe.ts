import { Food, Unit } from "./food";

export interface Recipe {
  id: string;
  name: string;
  description: string;
  ingredients: Array<Ingredient>;
  steps: Array<string>;
}

export interface Ingredient {
  food: Pick<Food, "id">;
  amount: number;
  unit: Unit;
}
