"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UnitClass = /** @class */ (function() {
  function UnitClass(key, isVolume, conversion) {
    if (isVolume === void 0) {
      isVolume = false;
    }
    this.key = key;
    this.isVolume = isVolume;
    this.conversion = conversion;
  }
  UnitClass.prototype.toString = function() {
    return this.key;
  };
  return UnitClass;
})();
exports.Unit = {
  GRAMS: new UnitClass("GRAMS"),
  MILLILITERS: new UnitClass("MILLILITERS", true, 1),
  CUP: new UnitClass("CUP", true, 236.59),
  SMALL_SLICE: new UnitClass("SMALL_SLICE"),
  MEDIUM_SLICE: new UnitClass("MEDIUM_SLICE"),
  LARGE_SLICE: new UnitClass("LARGE_SLICE"),
  SMALL: new UnitClass("SMALL"),
  MEDIUM: new UnitClass("MEDIUM"),
  LARGE: new UnitClass("LARGE"),
  TABLESPOON: new UnitClass("TABLESPOON", true, 14.79),
  TEASPOON: new UnitClass("TEASPOON", true, 4.93),
  WHOLE: new UnitClass("WHOLE")
};
//# sourceMappingURL=food.js.map
