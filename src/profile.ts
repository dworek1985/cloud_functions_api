export interface Profile {
  unitSystem: UnitSystem;
  height: number;
  weight: number;
  gender: Gender;
  birthday: Date;
  activityLevel: ActivityLevel;
  goal: Goal;
  nutrientsRequirements?: NutrientsRequirements;
}

export interface NutrientsRequirements {
  calories: NumberRange;
  fats: NumberRange;
  carbohydrates: NumberRange;
  proteins: NumberRange;
}

export interface NumberRange {
  from: number;
  to: number;
}

export enum UnitSystem {
  Metric = "M",
  Imperial = "I"
}

export enum Gender {
  Male = "M",
  Female = "F"
}

export const enum ActivityLevel {
  Sedentary = "S",
  Lightly = "L",
  Moderately = "M",
  Very = "V",
  Extremely = "E"
}

export const enum Goal {
  Loss = "L",
  Maintenance = "M",
  Gain = "G"
}
