"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UnitSystem;
(function(UnitSystem) {
  UnitSystem["Metric"] = "M";
  UnitSystem["Imperial"] = "I";
})((UnitSystem = exports.UnitSystem || (exports.UnitSystem = {})));
var Gender;
(function(Gender) {
  Gender["Male"] = "M";
  Gender["Female"] = "F";
})((Gender = exports.Gender || (exports.Gender = {})));
//# sourceMappingURL=profile.js.map
