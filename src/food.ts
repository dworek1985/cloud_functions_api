export interface Food {
  id: string;
  name: string;
  foodNutrients: FoodNutrients;
  measures: Measures;
}

export interface FoodNutrients<Value = number> {
  calories?: Value;
  // Fats
  fat?: Value;
  sat?: Value;
  mono?: Value;
  // Carbs
  carbs?: Value;
  sugar?: Value;
  fiber?: Value;
  // Protein
  protein?: Value;
}

export interface Measures {
  isVolume: boolean;
  volumeMl?: number;
  units: Array<MeasureConversion>;
}

export interface MeasureConversion {
  unit: Unit;
  grams: number;
}

class UnitClass {
  constructor(
    private readonly key: string,
    readonly isVolume: boolean = false,
    readonly conversion?: number
  ) {}

  toString() {
    return this.key;
  }
}

export const Unit = {
  GRAMS: new UnitClass("GRAMS"),
  MILLILITERS: new UnitClass("MILLILITERS", true, 1),
  CUP: new UnitClass("CUP", true, 236.59),
  SMALL_SLICE: new UnitClass("SMALL_SLICE"),
  MEDIUM_SLICE: new UnitClass("MEDIUM_SLICE"),
  LARGE_SLICE: new UnitClass("LARGE_SLICE"),
  SMALL: new UnitClass("SMALL"),
  MEDIUM: new UnitClass("MEDIUM"),
  LARGE: new UnitClass("LARGE"),
  TABLESPOON: new UnitClass("TABLESPOON", true, 14.79),
  TEASPOON: new UnitClass("TEASPOON", true, 4.93),
  WHOLE: new UnitClass("WHOLE")
};

export type Unit = keyof typeof Unit;
